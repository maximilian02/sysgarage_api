const express         = require('express');
const router          = express.Router();
const config          = require('./config/cfg');
const jwt             = require('jsonwebtoken');
const user_controller = require('./controllers/user');
const calc_controller = require('./controllers/calculate');

router.post('/authenticate', user_controller.authenticate);
router.post('/client', user_controller.create_client);

// Pre flight method not needed right now.
// Just skip it and return a 200 status
router.use((req, res, next) => {
    if (req.method === 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

router.use((req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

router.post('/admin', user_controller.create_admin);
router.post('/block', user_controller.block_user);
router.get('/prime/:num', calc_controller.prime);
router.post('/context', user_controller.context);

module.exports = router;
