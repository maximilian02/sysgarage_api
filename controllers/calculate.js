const isPrime = (num) => {
    const sqr = Math.floor(Math.sqrt(num));
    let prime = num != 1;
    for(let i = 2; i < sqr + 1; i++) {
        if(num % i == 0) {
            prime = false;
            break;
        }
    }
    return prime;
};

exports.prime = (req, res) => {
    if (!req.params.num) {
        res.send({ success: false, message: 'Missing required fields.' });
    } else {
        const isPrimeNum = isPrime(req.params.num);
        res.send({ success: true, message: `The number it ${isPrimeNum ? 'IS' : 'IS NOT'} a prime number.`, isPrime: isPrimeNum});
    }
};
