const db          = require('../config/db');
const config      = require('../config/cfg');
const User        = db.extend({ tableName: 'users' });
const credential  = require('credential');
const jwt         = require('jsonwebtoken');
const pw          = credential();


exports.block_user = (req, res) => {
    if (!req.body.user_id) {
        res.json({ success: false, message: 'Missing required fields.' });
    } else if (req.decoded.admin !== 1) {
        res.json({ success: false, message: 'You must have permissions to execute this operation.' });
    } else if (req.body.user_id == req.decoded.id) {
        res.json({ success: false, message: 'You are not able to delete your existence in this world.' });
    } else {
        const findUser = new User();
        findUser.find('first', { where: `id = ${req.body.user_id}` }, (err, row) => {
            if (err) throw err;
            const user = row ? row : null;

            if (!user) {
                res.json({ success: false, message: 'User not found.' });
            } else if (user.active === 0) {
                res.json({ success: false, message: 'The user is already blocked.'});
            }  else if (user) {
                findUser.setSQL(user);
                findUser.set('active', '0');
                findUser.save();
                res.json({ success: true, message: 'The user was succesfully blocked.'});
            }
        });
    }
};

exports.create_admin = (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.json({ success: false, message: 'Missing required fields.' });
    } else if (req.decoded.admin !== 1) {
        res.json({ success: false, message: 'You must have permissions to execute this operation.' });
    } else {
        let newAdminData = {
            email: req.body.email,
            name: req.body.name ? req.body.name : '',
            admin: 1,
            hash: null
        };

        pw.hash(req.body.password, (err, hash) => {
            if (err) { throw err; }

            newAdminData.hash = hash;
            const newAdmin = new User(newAdminData);
            newAdmin.save((err) => {
                if (err) { throw err; }
                console.log(`New ADMIN created (${req.body.email}) with password: ${req.body.password}`);
            });
        });

        res.json({ success: true, message: 'The admin was succesfully created.' });
    }
};

exports.create_client = (req, res) => {
    if (!req.body.name || !req.body.email || !req.body.password) {
        res.json({ success: false, message: 'Missing required fields.' });
    } else {
        let newClientData = {
            name: req.body.name,
            email: req.body.email,
            admin: 0,
            hash: null
        };

        pw.hash(req.body.password, (err, hash) => {
            if (err) { throw err; }

            newClientData.hash = hash;
            const newClient = new User(newClientData);
            newClient.save((err) => {
                if (err) { throw err; }
                console.log(`New CLIENT created (${req.body.email}) with password: ${req.body.password}`);
            });
        });

        res.json({ success: true, message: 'The client was succesfully created.' });
    }
};

exports.authenticate = (req, res) => {
    const findUser = new User();

    // TODO: The way of doing the concat email is not good because might allow SQL injection.
    // But at this moment i can't find any other way of searching the data using the mysql-models. Shouldn't be that hard, tho.
    findUser.find('first', { where: `email = '${req.body.email}'`}, (err, row) => {
        if (err) throw err;
        const user = row ? row : null;

        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
            pw.verify(user.hash, req.body.password, (err, isValid) => {
                if (err) { throw err; }

                if (!isValid) {
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                } else {
                    const payload = {
                        admin: user.admin,
                        id: user.id
                    };
                    const token = jwt.sign(payload, config.secret, { expiresIn: 1440 });

                    res.json({
                        success: true,
                        token: token
                    });
                }
            });
        }
    });
};

exports.context = (req, res) => {
    res.json({ success: true, message: 'User is already authenticated.' });
};
