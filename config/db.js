const mysqlModel = require('mysql-model');
const config = require('./cfg');

module.exports = mysqlModel.createConnection(config.database);
