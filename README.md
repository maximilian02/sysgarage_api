# README #

### How do I get set up? ###

* Make sure you properly import the database
* Check the config/cfg.js file and modify data according to your own system

Then you should be ready to run in the console:

* npm install
* npm run seed

And finally let the magic run:

* npm start