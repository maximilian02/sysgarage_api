const express     = require('express');
const app         = express();
const bodyParser  = require('body-parser');
const port        = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,x-access-token');
    next();
});

app.use(require('./routes'));

app.listen(port, () => {
    console.log(`Sysgarage test app running on port: ${port}`);
});
