const db = require('./config/db');
const User = db.extend({ tableName: 'users' });
const credential = require('credential');
const pw = credential();
const adminData = { email: 'admin@admin.com', admin: 1, hash: null };
const adminPassword = 'admin1';

// If admin user already exists will throw an error because of the unique index of email field.
pw.hash(adminPassword, (err, hash) => {
    if (err) { throw err; }
    adminData.hash = hash;
    const adminUser = new User(adminData);
    adminUser.save((err) => {
        if (err) { throw err; }
        console.log(`New ADMIN created (${adminData.email}) with password: ${adminPassword}`);
        process.exit();
    });
});
